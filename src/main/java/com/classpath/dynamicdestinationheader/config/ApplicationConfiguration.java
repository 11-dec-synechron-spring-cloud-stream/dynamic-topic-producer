package com.classpath.dynamicdestinationheader.config;

import org.springframework.cloud.stream.binder.BinderHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public Supplier<String> supplyForTargetDestination(){
        return () -> UUID.randomUUID().toString();
    }

    @Bean
    public Function<Message<String>, Message<String>> dynamicProducer() {
        System.out.println("Inside the dynamic producer :: ");
        return (message) -> {
            String topic = "topic/out/"+Math.round(Math.random()* 20000);
            return MessageBuilder
                        .withPayload(message.getPayload())
                        .setHeader(BinderHeaders.TARGET_DESTINATION, topic)
                    .build();

        };
    }

    @Bean
    public Consumer<String> processMessageConsumer(){
        return (data) -> {
            System.out.println("Received the message");
            System.out.println(data);
        };
    }

}