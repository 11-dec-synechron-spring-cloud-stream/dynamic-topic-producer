package com.classpath.dynamicdestinationheader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicDestinationHeaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicDestinationHeaderApplication.class, args);
    }

}
